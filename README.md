# WIP - Plan for the DarkPlaces Engine upgrade

This is a plan for modernising the DarkPlace engine powering Xonotic. Note that this is very much a work in progress, and any feedback is greatly appreciated. I would prefer if any question/suggestions was filed as issues [here](https://gitlab.com/Visse44/darkplaces-upgrade-plan/issues) for easy an overview.

#### Document History
| Version  | Description                    | Author    | Date       |
|----------|--------------------------------|-----------|------------|
|      0.1 | Initial draft                  | Visse     | 2019-03-15 |

#### Table of content
1. [Summary](#1-summary)
2. [Goals](#2-goals)
3. [How can you help?](#3-how-can-you-help)
4. [Future Goals and features](#4-future-goals-and-features)
5. [Plan](#5-plan)
6. [Open questions](#6-open-questions)

## 1. Summary
The engine is showing its age and its not getting better with time. The goal is document is to layout a plan for moving the engine, in small steps, towards a more modern architecture.

I don't expect anyone to volunteer and put in any work into realizing this document. That said, part of the purpose of this document is to try to invite people to help make the engine better.

This is not intended to be disrespectful/insulting towards the people currently developing and maintaining the engine. I believe that they have done a great job (and currently is) and, from the brief code dive I've done, the code is surprisingly easy to read and follow, especially once you consider that its 20(?) years old.

Part of moving to a more modern architecture is to move away from C, towards C++. I know that there are people that dislike C++, and much prefer working in C. The plan is NOT to move to a full C++ object oriented codebase, but I do believe that C++ brings in lots of quality of life improvements outside off classes, especially with the more modern variants of C++11 and forward.

A very import aspect of the plan is too keep backward compatibility with older clients and servers. Without that, this plan wouldn't work and at best fail with no adoption, at worse split the community, which I'm not sure that it would survive again.

This is also not intended to fix bugs that would change the behaviour (see backward compatibility), rather its to move the engine to a more modern architecture that is more performant, easier to maintain and develop new features for. This doesn't mean that no bugs will be fixed, but they need to be fixed with backward compatibility in mind.

#### What kind of feedback I'm looking for
* __Opinion__
    I'm interested what you think about this approach, are you against it?, support it? and more importantly why? If you are against, I would like to know what changes you would like to make.
* __Interesting in helping__
    If you are interesting in helping, in any way, I would like to know. It could be help with testing (by simple playing the game), implementing the changes, or simple provide input on design choices.
* __Other approaches__
    Do you have an idea of another approach, I would very much like to know it.

I have a hard time following discussions in chat, so I would greatly appreciate that questions & feedback to be filed as issues [here](https://gitlab.com/Visse44/darkplaces-upgrade-plan/issues). I believe that this would also make it easier to get an overview, and making sure that no feedback is lost.

#### Who is this document for?
This is mainly intended for me to have something to keep the changes structured and with a clear goal in mind.
Its also intended as a way to gather feedback and suggestions from people interested in the development of Xonotic. As a bonus, I hope that it might even motivate people to join in helping making the engine better.

#### Why modernise darkplaces?
* __OpenGL2 Renderer__
    The engine is using an renderer based on OpenGL2 and while I believe that where have been some effort to port it to a other APIS (such as DirectX, or OpenGL3+), but they seem to have failed (Please correct me if I'm wrong).
    OpenGL2 will only get slower with time, as more drivers drop direct support for it, and it instead have to be emulated. This can be seen with directX as older versions are becoming slower with newer drivers, even though the hardware is getting faster.
* __Single threaded__
    While the engine supports some threading (what I can tell to run server and client in the same process) it really needs a upgrade, as its heavily CPU bound. To fix this requires a new architecture and is out of scope for this plan, but, hopefully, by modernising and refactoring the engine it will be easier to implement in the future.
* __Increase the maintainability__
* __C code base__
    While its nothing inherently bad with C, I believe that moving to C++ would provide several benefits.
    - C++ has more "batteries" included than C
    - More developers are familiar with C++ than with C
    - Clearer code, especially with the later versions after C++11


###  Other approaches:
These are some other approaches that could be made, would be greatly appreciated if you think about another approach to mention it.

#### Do nothing / Status quo
The engine is showing its age and by doing nothing, it would keep ageing until it was no longer relevant.

#### Replace the Engine
I've heard that there have been some efforts to replace the engine. I don't know how its progressing, but I don't think that its the right approach. Replacing the engine require a lot of work to be done, before you can even see any result. There is also a high probability to introduce incompatibilities witch would be hard to track down, double so since its another codebase.

By instead improving the existing engine with small changes you get an improvements gradually and get a chance of catching regressions early. Its also easier to track down the cause, since the two codebases would be similar.

#### Rewrite
Rewriting the engine is not on the table. It would require huge amount of work, and would throw away all the work that have gone into the engine.

### When?
Currently I plan to start in June. This will hopefully be enough time to gather feedback and come up with a more detailed plan. It would also give time for other people that might be interested to prepare.

### Time?
With a single person working on it on their spare time, I would expect it to take between 6 to 12 months. Of course it could be longer (or quicker), since I haven't done any detailed analyse of what needs to be done, nor do I have any estimates on how long those tasks would take.



## 2. Goals
These are the primary goals that the plan is to address.

#### 1. Documentation
Documentation is very important, and a big part of this plan is to document how the engine works. The goal is to document everything, but that will probably be impossible. Instead some key sequences will be documented, such as

* Startup - How the engine starts, what order the different systems are brought up, files accessed, etc..
* MainLoop - How the engine process each tick/frame, what order the systems are updated
* Rendering - How the engine renders each frame
* Network - What the protocol that the engine uses for server-client communication, and how the state is shared between them.
* Physics - How the engine updates the physics simulation
* Shutdown - How are the different systems brought down?, are all resources freed?

I expect that some of that documentation already exists somewhere, but its unclear how accurate it is. And since accurate information is very important to the success of this plan, it will be extracted from how the code.

#### 2. Automated tests
In order to be able to efficiently develop the engine, its important that there are tests that can be run. I've not actively searched for any tests (and would greatly appreciate if anyone know of some), but as far as I can tell the engine doesn't really has very limited amount tests. Those I found is some unit tests in mathlib.c (that are run every time as part of engine startup), and some inline tests for the network code.
Part of the goal of this plan is to increase the number of tests to make it easier to make modifications to the code and still be reasonable sure that it still works afterward.

Adding unit tests to an existing codebase can be quite tricky and require extensive modifications to the code, instead I think an better approach would be to add a way to automate user input. This would allow for regression testing. Another way is to simulate a client/server on the network protocol level, and see that it responds in the right way.

#### 3. Refactoring
By refactoring the code I hope to prepare it for feature upgrades, such as multithreading and a move to modern OpenGL (or Vulkan).

#### 4. Move to C++
While I know that there are people that will oppose a move to C++, I believe that such a move would be for the best.  C++ brings lots new features (especially with C++11 and forward) that makes its easier and more straightforward to implement. From personal experience I find that C++ code is usually has fewer bugs, and is easier to write tests for.

#### 4. Network code
The network code could use some work. It currently don't handle the internet with its dropping and reordering of packets. It also teleport to fix correction of position, instead of interpolating, making it very annoying playing against players with unstable network.
The goal is to examine the protocol and implementation to see what you could do to improve it. No changes to the protocol is possible, for now, and no changes to the implementation that would cause it to become backward incompatible will be implemented.

Another part of the network code that could use some work is the download utility used primarily for map downloads. We will investigate whenever its possible to implement a hash sum check for the finished download, to make sure that its not corrupt.

#### 5. Unify command line arguments
Currently its no way to query what command lines argument is available. To change that, I propose that we collect all command lines arguments to a single place, similar to how cvars are handled by the engine. This would be implemented as an additional function in all systems to allow them to register the arguments that they handle. This would allow implementation of a useful '--help' switch and additional error checking of arguments (and issue warnings about unknown arguments).

#### 6. Lower the number if different configurations
Currently the code contains lots of different #if'defs, many of which are not documented. I believe that it would be good to try to lower that number. This would make it easier to maintain the engine.

#### 7. Prepare the renderer for a upgrade
This is one of the main objectives, and while I would like to see a full render upgrade in this plan, I believe that would be a to big of a goal.

#### 8. Change build system
This is a minor goal, but it would be nice to have a more modern build system. I would suggest moving to CMake or maybe [Meson](https://github.com/mesonbuild/meson). The benefit would be that it would be easier to work in other environments. For example CMake can output several different formats, from Make and Ninja, to Visual Studio projects. This would remove the need to support several different solutions.

#### 9. Add support for performance measure tools
With support of some good performance tools (well develop tools in general) its easier to get an overview over what the problem areas are, and what the bottle necks are. I would prefer adding support for [Remotery](https://github.com/Celtoys/Remotery) which I have pleasant experiences working with in the past. I would appreciate any recommendation on additional tools.

#### 10. Look over dependencies
As the title says, looking over all the dependencies. Many of whom have their entire source included in the build, others with heavily modified custom headers. The goal is to go thru them and see if newer versions are available, or if it can be removed alternative replaced/joined with another library. Another part is to move them out of the source to their own sub repositories making it easier to see what the dependencies are.



## 3. How can you help?
@todo
By just reading this document you already helped, giving feedback and asking questions would be the next logical step. It would also be helpful to spread this document to other people.

Later it would also be good if you were willing to help with regression testing by running the engine and playing games.



## 4. Future Goals and features
These are the features that I actually want / would be good to have, but they are too big and require lots of changes to how the engine works to be properly implemented. This plan can, in a way, be seen as a first step to prepare the engine for the features.

#### Upgrade renderer
Upgrading to modern OpenGL (OpenGL3+) or Vulkan.

#### Multithreading
From the brief look at the source it looks like the engine supports limited threaded (From what I can see its used for running client and server in the same process), but in order to support "proper" multithreading the engine needs a major overhaul of its architecture, which this plan is preparing for.

#### Support for additional scripting language
While I don't think that QuakeC can ever stop being supported, it would be nice to have a more modern language to script the engine in. I would suggest adding support for Lua, which have been widely adopted in the game industry.

#### Support "distributed" servers
I'm not quite sure what to call this feature, but my idea is that a single server is distributed over multiple physical ones. Each physical server is the 'master' of its client, but it is connected to the others and sends its clients to the others. I don't believe this would be useful in team based games, but in defrag this would mean that players from different continents could play on the same server, and have reasonable ping.
The closest thing currently supported is sharing chat between servers, but this is intended as a step further and be transparent to the players.
It could also be used for duel games - there multiple games could be played at once.

#### REST API
I understand that there is rcon(?) but to my understanding that's similar to SSH. The benefit of having a REST API for exposing server information / commands, is that there are a magnitude of tools and frameworks designed to work with it, and for most languages there is libraries for using REST APIs.



## 5. Plan:
@todo
- [ ] 1. Documentation + Tests
    - [ ] Get the code to build
    - [ ] Create Tests
    - [ ] Document
        - [ ] Startup sequence
        - [ ] Main loop
        - [ ] Network
        - [ ] Graphics
        - [ ] Dependencies
        - [ ] Resource managing
- [ ] 2. Cleanup & Preparations
    - [ ] Move to a more modern build system (CMake or Meson)
    - [ ] Prepare systems to move to C++
        - [ ] Audio
        - [ ] Graphics
        - [ ] Network
        - [ ] Physics
        - [ ] System (filesystem, timers...)
- [ ] 3. Move to C++



## 6. Open questions
None at the moment
